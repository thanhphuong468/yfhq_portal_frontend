FROM node:lts
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
RUN apt-get update && apt-get install -y vim
WORKDIR /home/node/app
USER node
COPY --chown=node:node package*.json ./
RUN npm install -f
COPY --chown=node:node . .
# RUN npm run build
# ENV NODE_ENV production
EXPOSE 3000
CMD ["npm", "start"]
