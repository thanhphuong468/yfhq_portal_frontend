#!/usr/bin/env bash
USER=ubuntu
HOST=18.221.188.61
PRIVATE_KEY=$(dirname $0)/server-credentials/yhcmute.pem

export DOCKER_HOST=ssh://$USER@$HOST

chmod 400 $PRIVATE_KEY
ssh-add $PRIVATE_KEY
