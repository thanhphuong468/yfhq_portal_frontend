#!/usr/bin/env bash
source $(dirname $0)/config.sh
USER=ubuntu
HOST=18.221.188.61

export DOCKER_HOST=ssh://$USER@$HOST

docker-compose build frontend
docker-compose up --no-deps -d frontend
