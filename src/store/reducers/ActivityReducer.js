import { toast } from 'react-toastify'
import { toastError, toastSuccess } from '../../helper/toastHelper'

const { createSlice } = require('@reduxjs/toolkit')

const ActivityReducer = createSlice({
  name: 'Activities',
  initialState: {
    ActivityList: [],
    Activity: {},
    totalActivity: 0,
    ActivityListForUSer: [],
    totalActivityForUser: 0,
    ActivityListForAdmin: [],
    ActivitiesRequestBase: [],
    totalRequest: 0,
    ActivityStudentRegister: [],
    ActivityStudentAttendance: [],
  },
  reducers: {
    fetchListActivity: () => {},
    fetchListActivitySuccess: (state, action) => {
      state.ActivityList = action.payload
    },
    fetchListActivityFail: (state, action) => {
      toastError(action.payload)
    },
    fetchtotalActivities: () => {},
    fetchtotalActivitiesSuccess: (state, action) => {
      state.totalActivity = action.payload
    },
    fetchtotalActivitiesFail: (state, action) => {
      toastError(action.payload)
    },
    fetchActivity: () => {},
    fetchActivitySuccess: (state, action) => {
      state.Activity = action.payload
    },
    fetchActivityFail: (state, action) => {
      toastError(action.payload)
    },
    fetchAddActivity: () => {},
    fetchAddActivitySuccess: (state, action) => {
      state.ActivityList.push(action.payload)
      toastSuccess('Đăng ký chương trình thành công')
    },
    fetchAddActivityFail: (state, action) => {
      const { error } = action.payload
      toastError(error)
    },
    fetchRegisterActivityForStudent: () => {},
    fetchRegisterActivityForStudentSucces: (state, action) => {
      if (action.payload === 'true')
        toastSuccess('Chương trình đăng ký thành công')
      else
        toast.error('Chương trình đã được đăng ký !', {
          theme: 'dark',
        })
    },
    fetchRegisterActivityForStudentFail: (state, action) => {
      const { error } = action.payload
      toastError(error)
    },
    fetchActivityForUser: () => {},
    fetchActivityForUserSuccess: (state, action) => {
      state.ActivityListForUSer = action.payload
    },
    fetchActivityForUserFail: (state, action) => {
      toastError(action.payload)
    },
    fetchTotalActivitiesForUser: () => {},
    fetchTotalActivitiesForUserSuccess: (state, action) => {
      state.totalActivityForUser = action.payload
    },
    fetchTotalActivitiesForUserFail: (state, action) => {
      toastError(action.payload)
    },
    fetchActivityForAdmin: () => {},
    fetchActivityForAdminSuccess: (state, action) => {
      state.ActivityListForAdmin = action.payload
    },
    fetchActivityForAdminFail: (state, action) => {
      toastError(action.payload)
    },
    fetchEditActivity: () => {},
    fetchEditActivitySuccess: () => {
      // state.ActivityList.push(action.payload)
      toastSuccess('Thao tác thành công !')
    },
    fetchEditctivityFail: (state, action) => {
      const { error } = action.payload
      toastError(error)
    },
    fetchDeleteActivity: () => {},
    fetchDeleteActivitySuccess: () => {
      // state.ActivityList.push(action.payload)
      toastSuccess('Xóa chương trình thành công !')
    },
    fetchDeleteActivityFail: (state, action) => {
      const { error } = action.payload
      toastError(error)
    },
    fetchTotalRequestActivity: () => {},
    fetchTotalRequestActivitySuccess: (state, action) => {
      state.totalRequest = action.payload
    },
    fetchTotalRequestActivityFail: (action) => {
      toastError(action.payload)
    },
    fetchActivityRequestForUserBase: () => {},
    fetchActivityRequestForUserBaseSuccess: (state, action) => {
      state.ActivitiesRequestBase = action.payload
    },
    fetchActivityRequestForUserBaseFail: (state, action) => {
      toastError(action.payload)
    },
    fetchStudentRegisterActivity: () => {},
    fetchStudentRegisterActivitySuccess: (state, action) => {
      state.ActivityStudentRegister = action.payload
    },
    fetchStudentRegisterActivityFail: (state) => {
      state.ActivityStudentRegister = [
        {
          name: '--------',
          mssv: '--------',
        },
      ]
      toastError('Chương trình hiện chưa có người đăng ký !')
    },
    fetchStudentAttdenceActivity: () => {},
    fetchStudentAttdenceActivitySuccess: (state, action) => {
      state.ActivityStudentAttendance = action.payload
    },
    fetchStudentAttdenceActivityFail: (state) => {
      state.ActivityStudentAttendance = [
        {
          name: '--------',
          mssv: '--------',
        },
      ]
      toastError('Chương trình hiện chưa có người đăng ký !')
    },
    fetchRestoreActivity: () => {},
    fetchRestoreActivitySuccess: (state, action) => {
      const list = [...state.ActivityListForAdmin]
      state.ActivityListForAdmin = list.filter(
        (item) => item._id !== action.payload
      )
    },
    fetchRestoreActivityFail: (action) => {
      toastError(action.payload)
    },
  },
})
export const {
  fetchListActivity,
  fetchListActivitySuccess,
  fetchListActivityFail,
  fetchActivity,
  fetchActivitySuccess,
  fetchActivityFail,
  fetchtotalActivities,
  fetchtotalActivitiesSuccess,
  fetchtotalActivitiesFail,
  fetchAddActivity,
  fetchAddActivitySuccess,
  fetchAddActivityFail,
  fetchRegisterActivityForStudent,
  fetchRegisterActivityForStudentSucces,
  fetchRegisterActivityForStudentFail,
  fetchActivityForUser,
  fetchActivityForUserSuccess,
  fetchActivityForUserFail,
  fetchTotalActivitiesForUser,
  fetchTotalActivitiesForUserSuccess,
  fetchTotalActivitiesForUserFail,
  fetchActivityForAdmin,
  fetchActivityForAdminSuccess,
  fetchActivityForAdminFail,
  fetchEditActivity,
  fetchEditActivitySuccess,
  fetchEditctivityFail,
  fetchDeleteActivity,
  fetchDeleteActivitySuccess,
  fetchDeleteActivityFail,
  fetchTotalRequestActivity,
  fetchTotalRequestActivitySuccess,
  fetchTotalRequestActivityFail,
  fetchActivityRequestForUserBase,
  fetchActivityRequestForUserBaseSuccess,
  fetchActivityRequestForUserBaseFail,
  fetchStudentRegisterActivity,
  fetchStudentRegisterActivitySuccess,
  fetchStudentRegisterActivityFail,
  fetchStudentAttdenceActivity,
  fetchStudentAttdenceActivitySuccess,
  fetchStudentAttdenceActivityFail,
  fetchRestoreActivity,
  fetchRestoreActivityFail,
  fetchRestoreActivitySuccess,
} = ActivityReducer.actions
export default ActivityReducer.reducer
