import { toastError } from '../../helper/toastHelper'

const { createSlice } = require('@reduxjs/toolkit')

const AdminReducer = createSlice({
  name: 'Admin',
  initialState: {
    infoHome: {},
  },
  reducers: {
    fetchInfoHomePage: () => {},
    fetchInfoHomePageSuccess: (state, action) => {
      state.infoHome = action.payload
    },
    fetchInfoHomePageFail: (state, action) => {
      toastError(action.payload)
    },
  },
})

export const {
  fetchInfoHomePage,
  fetchInfoHomePageSuccess,
  fetchInfoHomePageFail,
} = AdminReducer.actions
export default AdminReducer.reducer
