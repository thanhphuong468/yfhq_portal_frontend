import { Button, TextField } from '@mui/material'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { fetchChangePassword } from 'store/reducers/AuthReducer'
import './ChangePassword.css'

const ChangePassword = () => {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    reset,
    getValues,
  } = useForm({
    mode: 'onChange',
  })
  const [repeatPassword, setRepeatPassword] = useState('')
  const [match, setMatch] = useState(false)
  const dispatch = useDispatch()

  const onHandleSubmit = (e) => {
    dispatch(fetchChangePassword(e))
    reset()
    setRepeatPassword(' ')
  }

  return (
    <div className="change-password">
      <div className="grid wide">
        <div className="cp-container">
          <p className="cp-title">Change Password</p>
          <form onSubmit={handleSubmit(onHandleSubmit)} className="cp-form">
            <TextField
              fullWidth
              {...register('oldPassword', {
                required: 'This input is required',
              })}
              id="outlined-required"
              label="Mật khẩu cũ"
              margin="dense"
              error={errors.oldPassword?.message.length > 0}
              helperText={errors.oldPassword && errors.oldPassword?.message}
              style={{ width: '100%' }}
              type="password"
            />
            <TextField
              fullWidth
              {...register('newPassword', {
                required: 'This input is required',
              })}
              id="outlined-required"
              label="Mật khẩu mới"
              margin="dense"
              error={errors.newPassword?.message.length > 0}
              helperText={errors.newPassword && errors.newPassword?.message}
              style={{ width: '100%' }}
              type="password"
            />
            <TextField
              fullWidth
              value={repeatPassword}
              id="outlined-required"
              label="Nhập lại mật khẩu mới"
              margin="dense"
              onChange={(e) => {
                setRepeatPassword(e.target.value)
                if (e.target.value !== getValues('newPassword')) {
                  setMatch(true)
                } else {
                  setMatch(false)
                }
              }}
              error={match}
              helperText={match && 'Password is not matched'}
              style={{ width: '100%' }}
              type="password"
            />
            <div className="btn_upload_submit">
              <Button
                type="submit"
                variant="contained"
                size="medium"
                disabled={!isValid}
              >
                Đổi mật khẩu
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ChangePassword
