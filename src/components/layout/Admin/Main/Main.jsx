/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import AutoGraphIcon from '@mui/icons-material/AutoGraph'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import { DateRangePicker } from '@mui/x-date-pickers-pro/DateRangePicker'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { CreateActivity, CreateNews, NotFound } from 'components/layout'
import moment from 'moment'
import PropTypes from 'prop-types'
import React, { useEffect, useState } from 'react'
import Chart from 'react-apexcharts'
import { FaBars } from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import { fetchInfoHomePage } from 'store/reducers/AdminReducer'
import { getTimeForWeek } from 'utils/time'
import { CreateSlider } from '../CreateSlider'
import { ListActivity } from '../ListAcitivity'
import { ListNews } from '../ListNews'
import { ListSlider } from '../ListSlider'
import { ListUsers } from '../ListUsers'
import { Topbar } from '../Topbar'
import './Main.css'

const Main = ({ handleToggleSidebar, path }) => {
  const info = useSelector((state) => state.admin.infoHome)
  const dispatch = useDispatch()
  const [value, setValue] = useState([
    moment(new Date().toDateString()).subtract(7, 'days'),
    new Date(),
  ])

  useEffect(() => {
    dispatch(fetchInfoHomePage())
  }, [])

  const series = [
    {
      name: 'News',
      data: info?.analyticsNews,
    },
    {
      name: 'Users',
      data: info?.analyticsUsers,
    },
    {
      name: 'Activities',
      data: info?.analyticsActivities,
    },
  ]

  const options = {
    chart: {
      height: 380,
      type: 'area',
      stacked: true,
      scroller: {
        enabled: true,
      },
      events: {
        selection(chart, e) {
          console.log(new Date(e.xaxis.min))
        },
      },
    },
    colors: ['#008FFB', '#00E396', '#CED4DC'],
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth',
    },
    fill: {
      gradient: {
        enabled: true,
        opacityFrom: 0.6,
        opacityTo: 0.8,
      },
    },
    legend: {
      position: 'top',
      horizontalAlign: 'left',
    },
    xaxis: {
      type: 'datetime',
      categories: getTimeForWeek(value),
    },
  }
  const renderHomeAdmin = () => {
    return (
      <div className="admin_home">
        <div className="grid wide ah">
          <div className="row">
            <div className="col l-4 m-6 c-12">
              <div className="ah_item">
                <p className="ah_text">Users</p>
                <div className="ah_num">
                  <div className="ah-left">
                    <span>{info?.usersForWeek}</span> &nbsp;
                    <span>This week</span>
                    <p className="ah-total">Total {info?.totalUsers}</p>
                  </div>
                  <AutoGraphIcon style={{ color: '#5db9ef' }} />
                </div>
              </div>
            </div>
            <div className="col l-4 m-6 c-12">
              <div className="ah_item">
                <p className="ah_text">News</p>
                <div className="ah_num">
                  <div className="ah-left">
                    <span>{info?.newForWeek}</span> &nbsp;
                    <span>This week</span>
                    <p className="ah-total">Total {info?.totalNews}</p>
                  </div>
                  <AutoGraphIcon style={{ color: '#5db9ef' }} />
                </div>
              </div>
            </div>
            <div className="col l-4 m-6 c-12">
              <div className="ah_item">
                <p className="ah_text">Activities</p>
                <div className="ah_num">
                  <div className="ah-left">
                    <span>{info?.activitiesForWeek}</span> &nbsp;
                    <span>This week</span>
                    <p className="ah-total">Total {info?.totalActivities}</p>
                  </div>
                  <AutoGraphIcon style={{ color: '#5db9ef' }} />
                </div>
              </div>
            </div>
          </div>
          <div className="ah-pick">
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DateRangePicker
                startText="Start date"
                endText="End date"
                value={value}
                onChange={(newValue) => {
                  dispatch(fetchInfoHomePage(newValue))
                  setValue(newValue)
                }}
                renderInput={(startProps, endProps) => (
                  <React.Fragment>
                    <TextField {...startProps} />
                    <Box sx={{ mx: 2 }}> to </Box>
                    <TextField {...endProps} />
                  </React.Fragment>
                )}
              />
            </LocalizationProvider>
          </div>
          <div className="ah-chart">
            <Chart type="area" options={options} series={series} height="90%" />
          </div>
        </div>
      </div>
    )
  }
  const renderComponent = () => {
    switch (path) {
      case '/dashboard/news':
        return <ListNews type="get-all" />
      case '/dashboard/request-news':
        return <ListNews type="get-request" />
      case '/dashboard/create-news':
        return <CreateNews />
      case '/dashboard/create-activity':
        return <CreateActivity />
      case '/dashboard/request-activity':
        return <ListActivity type="get-request" />
      case '/dashboard/activity':
        return <ListActivity type="get-all" />
      case '/dashboard/users':
        return <ListUsers type="get-all" />
      case '/dashboard/sliders':
        return <ListSlider />
      case '/dashboard/add-slider':
        return <CreateSlider />
      case '/dashboard/users-student':
        return <ListUsers type="get-user-student" />
      case '/dashboard/users-unionbase':
        return <ListUsers type="get-user-unionbase" />
      case '/dashboard/delete-news':
        return <ListNews type="delete-news" />
      case '/dashboard/delete-activity':
        return <ListActivity type="delete-activity" />
      case '/dashboard':
        return renderHomeAdmin()
      default:
        return <NotFound />
    }
  }
  return (
    <main>
      <div className="btn-toggle" onClick={() => handleToggleSidebar(true)}>
        <FaBars />
      </div>
      <Topbar />
      {renderComponent()}
    </main>
  )
}

Main.propTypes = {
  handleToggleSidebar: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
}

export default Main
