/* eslint-disable new-cap */
/* eslint-disable react/prop-types */
import Icon from '@material-ui/icons/Apps'
import CancelIcon from '@mui/icons-material/Cancel'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import ListAltIcon from '@mui/icons-material/ListAlt'
import QrCodeIcon from '@mui/icons-material/QrCode'
import VerifiedIcon from '@mui/icons-material/Verified'
import { Modal } from '@mui/material'
import IconButton from '@mui/material/IconButton'
import { Box } from '@mui/system'
import { useConfirm } from 'material-ui-confirm'
import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import QRCode from 'react-qr-code'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchActivityForAdmin,
  fetchDeleteActivity,
  fetchRestoreActivity,
} from '../../../../store/reducers/ActivityReducer'
import ModalListAttendance from '../ModalListAttendance/ModalListAttendance'
import './ListActivity.css'
// eslint-disable-next-line import/order
import RestoreIcon from '@mui/icons-material/Restore'

const customStyles = {
  headRow: {
    style: {
      border: 'none',
    },
  },
  headCells: {
    style: {
      color: '#202124',
      fontSize: '16px',
      fontWeight: 500,
    },
  },
  rows: {
    highlightOnHoverStyle: {
      backgroundColor: 'rgba(21, 151, 229, 0.2)',
      borderBottomColor: '#FFFFFF',
      borderRadius: '25px',
      outline: '1px solid #FFFFFF',
    },
    style: {
      fontSize: '14px',
    },
  },
  pagination: {
    style: {
      border: 'none',
    },
  },
}

function ListActivity({ type }) {
  const [open, setOpen] = useState(false)
  const [slug, setSlug] = useState()
  const [idActivity, setIdActivity] = useState('')
  const [openQr, setOpenQr] = useState(false)
  const handleOpen = (id) => {
    setOpen(true)
    setSlug(id)
  }
  const handleClose = () => setOpen(false)
  const data = useSelector((state) => state.activities.ActivityListForAdmin)
  const dispatch = useDispatch()
  const [pending, setPending] = useState(true)

  // const ExpandedComponent = ({ data }) => (
  //   <pre>{JSON.stringify(data, null, 2)}</pre>
  // )
  const confirm = useConfirm()
  const onHandleDeleteActivity = (id) => {
    confirm({
      description: `Bạn muốn xóa chương trình này ${id}?`,
      title: 'Xác nhận chương trình',
    })
      .then(() => {
        dispatch(fetchDeleteActivity(id))
      })
      .catch(() => console.log('Deletion cancelled.'))
  }

  const onHandleRestore = (id, title) => {
    confirm({
      description: `Bạn muốn khôi phục hoạt động ${title}?`,
      title: 'Xác nhận bài viết',
    })
      .then(() => {
        dispatch(fetchRestoreActivity(id))
      })
      .catch(() => console.log('Deletion cancelled.'))
  }

  const columnsGetAll = [
    {
      cell: () => <Icon style={{ fill: 'rgba(21, 151, 229, 0.5)' }} />,
      width: '56px', // custom width for icon button
      style: {
        borderBottom: '1px solid #FFFFFF',
        marginBottom: '-1px',
      },
    },
    {
      name: 'Name',
      selector: (row) => row.nameActivity,
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Start Date',
      selector: (row) => new Date(row.startDate).toLocaleDateString(),
      sortable: true,
      grow: 0.5,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'End Date',
      selector: (row) => new Date(row.endDate).toLocaleDateString(),
      sortable: true,
      center: true,
      grow: 0.5,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Place',
      selector: (row) => row.place,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Thumnail',
      cell: (row) => (
        <a
          className="thumbnailTable"
          href={row.thumbnail}
          target="_blank"
          rel="noreferrer"
        >
          Ấn vào để xem
        </a>
      ),
      // selector: (row) => row.thumbnail,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'User Create',
      selector: (row) => row.userCreate,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Tag',
      selector: (row) => row.tag,
      sortable: true,
      center: true,
    },
    {
      name: 'Danh sách sinh viên',
      grow: 1.5,
      cell: (row) => (
        <>
          <IconButton
            onClick={() => {
              handleOpen(row.slug)
            }}
            aria-label="edit"
            color="primary"
          >
            <ListAltIcon />
          </IconButton>
        </>
      ),
      width: '170px',
      button: true,
    },
    {
      name: 'QR Code',
      selector: (row) => (
        <>
          {row.status ? (
            <IconButton
              aria-label="edit"
              color="primary"
              onClick={() => {
                setIdActivity(row._id)
                setOpenQr(true)
              }}
            >
              <QrCodeIcon />
            </IconButton>
          ) : (
            ''
          )}
        </>
      ),

      sortable: true,
      center: true,
    },
    {
      cell: (row) => (
        <>
          {type === 'get-all' && (
            <>
              <IconButton
                href={`activity/editActivity/${row.slug}`}
                aria-label="edit"
                color="primary"
              >
                <EditIcon />
              </IconButton>
              <IconButton
                aria-label="delete"
                className="list_news_admin_btn_delete"
                onClick={() => {
                  onHandleDeleteActivity(row._id)
                }}
              >
                <DeleteIcon />
              </IconButton>
            </>
          )}
          {type === 'delete-activity' && (
            <>
              <IconButton
                aria-label="edit"
                color="primary"
                onClick={() => {
                  onHandleRestore(row._id, row.nameActivity)
                }}
              >
                <RestoreIcon />
              </IconButton>
            </>
          )}
        </>
      ),
      button: true,
    },
  ]

  const columnsGetRequest = [
    {
      cell: () => <Icon style={{ fill: 'rgba(21, 151, 229, 0.5)' }} />,
      width: '56px', // custom width for icon button
      style: {
        borderBottom: '1px solid #FFFFFF',
        marginBottom: '-1px',
      },
    },
    {
      name: 'Name',
      selector: (row) => row.nameActivity,
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Start Date',
      selector: (row) => new Date(row.startDate).toLocaleDateString(),
      sortable: true,
      grow: 0.5,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'End Date',
      selector: (row) => new Date(row.endDate).toLocaleDateString(),
      sortable: true,
      center: true,
      grow: 0.5,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Place',
      selector: (row) => row.place,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Thumnail',
      cell: (row) => (
        <a
          className="thumbnailTable"
          href={row.thumbnail}
          target="_blank"
          rel="noreferrer"
        >
          Ấn vào để xem
        </a>
      ),
      grow: 1.5,
      // selector: (row) => row.thumbnail,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'User Create',
      selector: (row) => row.userCreate,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Tag',
      selector: (row) => row.tag,
      sortable: true,
      center: true,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      cell: (row) => (
        <>
          <IconButton
            aria-label="edit"
            color="primary"
            href={`/dashboard/activity/request-activity/${row.slug}`}
          >
            <VerifiedIcon />
          </IconButton>
        </>
      ),
      button: true,
    },
  ]

  useEffect(() => {
    if (type === 'get-request') {
      dispatch(fetchActivityForAdmin({ status: false }))
    } else if (type === 'delete-activity') {
      dispatch(fetchActivityForAdmin({ deleted: true }))
    } else {
      dispatch(fetchActivityForAdmin())
    }
    const timeout = setTimeout(() => {
      setPending(false)
    }, 2000)
    return () => clearTimeout(timeout)
  }, [type])

  return (
    <div className="admin_list_news">
      <ModalListAttendance open={open} handleClose={handleClose} slug={slug} />
      <div className="grid wide">
        <div className="admin_list_container">
          <DataTable
            title={
              type === 'get-all'
                ? 'Danh sách chương trình'
                : 'Yêu cầu phê duyệt chương trình'
            }
            columns={type === 'get-request' ? columnsGetRequest : columnsGetAll}
            data={data}
            pagination
            // expandableRows
            // expandableRowsComponent={ExpandedComponent}
            customStyles={customStyles}
            highlightOnHover
            pointerOnHover
            progressPending={pending}
          />
        </div>
      </div>
      <Modal
        open={openQr}
        onClose={() => {
          setOpenQr(false)
        }}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="modal_edit_qr">
          <IconButton
            className="modal_container_base_close_btn"
            onClick={() => {
              setOpenQr(false)
            }}
          >
            <CancelIcon fontSize="medium" />
          </IconButton>
          <p className="aln-title">QR code hoạt động</p>
          <div className="aln-qr-container">
            <QRCode value={idActivity} />
          </div>
        </Box>
      </Modal>
    </div>
  )
}

export default ListActivity
