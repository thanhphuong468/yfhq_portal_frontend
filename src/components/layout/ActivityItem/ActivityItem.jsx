/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react'
import './ActivityItem.css'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import { useDispatch } from 'react-redux'
import { showLoading, closeLoading } from 'store/reducers/LoadingSlide'
import { ModalConfirm } from '../ModalConfirm/index'
import activityApi from '../../../api/ActivityApi'

function Activity(ActivityItem) {
  const dispatch = useDispatch()
  const roleUser = JSON.parse(localStorage.getItem('authLogin'))?.user?.role
  const userId = JSON.parse(localStorage.getItem('authLogin'))?.user?.id
  const [isRegister, setIsRegister] = useState(false)
  const [openModalConfirm, setOpenModalConfirm] = useState(false)
  const [idActivityRegister, setIdActivityRegister] = useState('')

  useEffect(() => {
    if (userId) {
      const checkingRegister =
        ActivityItem?.ActivityItem?.participatingList?.find(
          ({ idUserRegister }) => idUserRegister === userId
        )
      if (checkingRegister) setIsRegister(true)
    }
  }, [ActivityItem, userId])
  const onHandleRegisterActivity = async (idActivity) => {
    dispatch(showLoading())
    if (localStorage.getItem('authLogin')) {
      const resRegister = await activityApi.registerActivityForStudent({
        Id_Activity: idActivity,
        idUserRegister: userId,
      })
      if (resRegister?.status === 200) {
        if (isRegister === false) setIsRegister(true)
        toast.success('Bạn đã đăng ký thành công chương trình')
      } else {
        toast.error(
          'Thông báo',
          'Bạn đănh ký không thành công, vui lòng kiểm tra lại kết nối !'
        )
      }
    } else {
      window.location.replace('/login')
    }
    dispatch(closeLoading())
    setOpenModalConfirm(false)
  }
  return (
    <>
      <div className="col l-6 m-12 c-12 ">
        <div className="activity_content">
          <div className="row">
            <div className="col c-12 m-4 l-4">
              <div className="activity_content--picture">
                <img
                  className="activity_content--pic"
                  src={ActivityItem.ActivityItem.thumbnail}
                  alt="anh ne"
                />
              </div>
            </div>
            <div className="col c-12 m-8 l-8 center">
              <div className="activity_content--text">
                <div className="activity_content--header">
                  <div className="row">
                    <div className="col c-12 l-6 m-6">
                      <div className="activity_content--headertext">
                        {ActivityItem.ActivityItem.nameActivity}
                      </div>
                    </div>
                    <div className="col c-12 l-6 m-6">
                      <div className="activity_content_btn">
                        <Link
                          to={`/activity/${ActivityItem.ActivityItem.slug}`}
                          className="activityitem_btn-deteil"
                        >
                          Chi tiết
                        </Link>
                        {roleUser === 'user:unionBase' ||
                        roleUser === 'admin' ? (
                          <div
                            className="activityitem_btn-register"
                            onClick={() => {
                              toast.error(
                                'Chức năng này không thuộc phân quyền của User'
                              )
                            }}
                          >
                            Đăng ký
                          </div>
                        ) : (
                          <>
                            {isRegister ? (
                              <div
                                className="activityitem_btn-register"
                                style={{
                                  backgroundColor: '#bcbec2',
                                  hover: 'enabled',
                                }}
                              >
                                Đã đăng ký
                              </div>
                            ) : (
                              <div
                                className="activityitem_btn-register"
                                onClick={() => {
                                  setIdActivityRegister(
                                    ActivityItem.ActivityItem._id
                                  )
                                  setOpenModalConfirm(true)
                                }}
                              >
                                Đăng ký
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="activity_content--text--underline" />
                <div className="activity_content--text--des">
                  <b>Thời gian: </b>{' '}
                  {new Date(
                    ActivityItem.ActivityItem.startDate
                  ).toLocaleDateString()}{' '}
                  đến{' '}
                  {new Date(
                    ActivityItem.ActivityItem.endDate
                  ).toLocaleDateString()}
                </div>
                <div className="activity_content--text--des">
                  <b>Địa điểm:</b> {ActivityItem.ActivityItem.place}
                </div>
                <div className="activity_content--text--des">
                  <b>Quyền lợi:</b> {ActivityItem.ActivityItem.benefit}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ModalConfirm
        onOpen={openModalConfirm}
        onClose={() => setOpenModalConfirm(false)}
        title="ĐĂNG KÝ"
        content="Bạn có muốn đăng ký chương trình này ?"
        onOk={() => onHandleRegisterActivity(idActivityRegister)}
      />
    </>
  )
}

export default Activity
