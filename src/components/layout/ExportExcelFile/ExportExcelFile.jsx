import Icon from '@material-ui/icons/Apps'
import React, { useState, useEffect } from 'react'
import { Box } from '@mui/material'
import Modal from '@mui/material/Modal'
import DataTable from 'react-data-table-component'
import DataTableExtensions from 'react-data-table-component-extensions'
import 'react-data-table-component-extensions/dist/index.css'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchStudentRegisterActivity,
  fetchStudentAttdenceActivity,
} from '../../../store/reducers/ActivityReducer'
import './ExportExcelFile.css'

const customStyles = {
  headRow: {
    style: {
      border: 'none',
    },
  },
  headCells: {
    style: {
      color: '#202124',
      fontSize: '16px',
      fontWeight: 500,
    },
  },
  rows: {
    highlightOnHoverStyle: {
      backgroundColor: 'rgba(21, 151, 229, 0.2)',
      borderBottomColor: '#FFFFFF',
      borderRadius: '25px',
      outline: '1px solid #FFFFFF',
    },
    style: {
      fontSize: '14px',
    },
  },
  pagination: {
    style: {
      border: 'none',
      backgroundColor: 'transparent',
    },
  },
}

// eslint-disable-next-line react/prop-types
const ExportExcelFile = ({
  typeData,
  isOpen,
  handelClose,
  slug,
  nameActivity,
}) => {
  // const [openExportExcel, setOpenExportExcel] = useState(false)
  const [pending, setPending] = useState(true)
  const dataTableRegister = useSelector(
    (state) => state.activities.ActivityStudentRegister
  )
  const dataTableAttendance = useSelector(
    (state) => state.activities.ActivityStudentAttendance
  )
  console.log(typeData)
  console.log(slug)
  const [tableData, setTableData] = useState()
  const dispatch = useDispatch()

  const columns = [
    {
      name: 'Họ và Tên',
      selector: 'name',
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'MSSV',
      selector: 'mssv',
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
  ]
  useEffect(() => {
    if (typeData === 'register') {
      setTableData({
        columns,
        data: dataTableRegister,
        fileName: `DS_SV_DANG_KY_${nameActivity}`,
        exportHeaders: true,
      })
    } else {
      setTableData({
        columns,
        data: dataTableAttendance,
        fileName: `DS_SV_DIEM_DANH_${nameActivity}`,
        exportHeaders: true,
      })
    }
  }, [dataTableRegister])
  useEffect(() => {
    if (typeData === 'register') {
      dispatch(fetchStudentRegisterActivity(slug))
    } else {
      dispatch(fetchStudentAttdenceActivity(slug))
    }
    const timeout = setTimeout(() => {
      setPending(false)
    }, 2000)
    return () => clearTimeout(timeout)
  }, [typeData, slug])

  const columnsExportExcell = [
    {
      cell: () => <Icon style={{ fill: 'rgba(21, 151, 229, 0.5)' }} />,
      width: '50px',
      style: {
        borderBottom: '1px solid #FFFFFF',
        marginBottom: '-1px',
      },
    },
    {
      name: 'Họ và tên',
      selector: (row) => row?.name,
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
        font: 'Times New Roman',
      },
    },
    {
      name: 'MSSV',
      selector: (row) => row?.mssv,
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
        font: 'Times New Roman',
      },
    },
  ]

  // const handleCloseExportExcel = () => {
  //   setOpenExportExcel(false)
  // }

  return (
    <Modal
      open={isOpen}
      onClose={handelClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="modal_export_excel">
        <DataTableExtensions {...tableData}>
          <DataTable
            columns={columnsExportExcell}
            data={tableData}
            pagination
            customStyles={customStyles}
            highlightOnHover
            pointerOnHover
            progressPending={pending}
          />
        </DataTableExtensions>
      </Box>
    </Modal>
  )
}
export default ExportExcelFile
