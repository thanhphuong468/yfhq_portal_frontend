/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react'
import './GeneralActivity.css'
import Pagination from '@mui/material/Pagination'
import { Button, TextField, useMediaQuery } from '@mui/material'
import {
  fetchListActivity,
  fetchtotalActivities,
} from 'store/reducers/ActivityReducer'

import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { showLoading, closeLoading } from 'store/reducers/LoadingSlide'
import { ActivityItem } from '../ActivityItem'
// import NotFound from '../NotFound/NotFound'

function GeneralActivity() {
  const listActivity = useSelector((state) => state.activities.ActivityList)
  const totalActivity = useSelector((state) => state.activities.totalActivity)
  const [dataStartDate, setDataStartDate] = useState('')
  const [dataEndDate, setDataEndDate] = useState('')
  const totalPage = Math.ceil(totalActivity / 10)
  const matches = useMediaQuery('(max-width:739px)')
  const dispatch = useDispatch()
  const { slug } = useParams()
  useEffect(() => {
    dispatch(showLoading())
    dispatch(
      fetchListActivity({
        page: 1,
        limit: 10,
        slug,
        startDate: dataStartDate,
        endDate: dataEndDate,
      })
    )
    dispatch(
      fetchtotalActivities({
        slug,
        startDate: dataStartDate,
        endDate: dataEndDate,
      })
    )
    setTimeout(() => {
      dispatch(closeLoading())
    }, 1500)
  }, [dataStartDate, dataEndDate])

  window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth',
  })

  const onHandleChangePage = (event, value) => {
    dispatch(showLoading())
    dispatch(
      fetchListActivity({
        page: value,
        limit: 10,
        slug,
        startDate: dataStartDate,
        endDate: dataEndDate,
      })
    )
    setTimeout(() => {
      dispatch(closeLoading())
    }, 1500)
  }
  const {
    register,
    formState: { errors },
    // reset,
    handleSubmit,
    // isValid,
  } = useForm({
    mode: 'onChange',
  })
  const onHanldeSubmit = (e) => {
    setDataStartDate(e?.startDate)
    setDataEndDate(e?.endDate)
  }
  return (
    <div className="generalActivity">
      <div className="grid wide">
        <div className="generalActivity_slider">
          <div className="generalActivity_slider-text">
            &quot; Chuyển đổi số - Digital Transformation in HCMUTE &quot;
          </div>
        </div>
        <div className="generalActivity_content">
          <div className="generalActivity_content-title">
            Tổng hợp chương trình
          </div>
          <form onSubmit={handleSubmit(onHanldeSubmit)}>
            <div className="studentActivity_fillter">
              <TextField
                className="studentActivity_date-picker"
                {...register('startDate', {
                  required: 'This input is required',
                })}
                id="date-start"
                label="Ngày bắt đầu"
                type="date"
                defaultValue={new Date()}
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
                error={errors.startDate}
                helperText={errors.startDate && errors.startDate?.message}
              />
              <TextField
                className="studentActivity_date-picker"
                {...register('endDate', {
                  required: 'This input is required',
                })}
                id="date-end"
                label="Ngày kết thúc"
                type="date"
                defaultValue={new Date()}
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
                error={errors.endDate}
                helperText={errors.endDate && errors.endDate?.message}
              />
              <Button
                type="submit"
                className="studentActivity_button-fillter"
                variant="contained"
              >
                Lọc
              </Button>
            </div>
          </form>
          <div className="row">
            {listActivity.map((item) => (
              <ActivityItem key={item._id} ActivityItem={item} />
            ))}
          </div>
          <Pagination
            size={matches ? 'small' : 'medium'}
            count={totalPage}
            className="generalAnnouncement_pagination"
            color="primary"
            variant="outlined"
            onChange={onHandleChangePage}
          />
        </div>
      </div>
    </div>
  )
}

export default GeneralActivity
