/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { toast } from 'react-toastify'
import { showLoading, closeLoading } from 'store/reducers/LoadingSlide'
import { NotFound } from '..'
import { fetchActivity } from '../../../store/reducers/ActivityReducer'
import { HotNewsItem } from '../HotNewsItem'
import activityApi from '../../../api/ActivityApi'
import { ModalConfirm } from '../ModalConfirm/index'
import './ActivityDetail.css'

function ActivityDetail() {
  const Activity = useSelector((state) => state.activities.Activity)
  const roleUser = JSON.parse(localStorage.getItem('authLogin'))?.user?.role
  const dispatch = useDispatch()
  const { slug } = useParams()
  const userId = JSON.parse(localStorage.getItem('authLogin'))?.user?.id
  const [isRegister, setIsRegister] = useState(false)
  const [openModalConfirm, setOpenModalConfirm] = useState(false)
  const [idActivityRegister, setIdActivityRegister] = useState('')
  useEffect(() => {
    if (userId) {
      const checkingRegister = Activity?.participatingList?.find(
        ({ idUserRegister }) => idUserRegister === userId
      )
      if (checkingRegister) setIsRegister(true)
      else setIsRegister(false)
    }
  }, [Activity, userId])

  useEffect(() => {
    window.scrollTo(0, 0)
    dispatch(fetchActivity(slug))
  }, [])
  const onHandleRegisterActivity = async (idActivity) => {
    dispatch(showLoading())
    if (localStorage.getItem('authLogin')) {
      const resRegister = await activityApi.registerActivityForStudent({
        Id_Activity: idActivity,
        idUserRegister: userId,
      })
      if (resRegister?.status === 200) {
        if (isRegister === false) setIsRegister(true)
        toast.success('Bạn đã đăng ký thành công chương trình')
      } else {
        toast.error(
          'Thông báo',
          'Bạn đănh ký không thành công, vui lòng kiểm tra lại kết nối !'
        )
      }
    } else {
      window.location.replace('/login')
    }
    dispatch(closeLoading())
    setOpenModalConfirm(false)
  }

  if (Activity === null) {
    return <NotFound />
  }
  if (Object.keys(Activity).length !== 0) {
    return (
      <div className="Activities">
        <div className="grid wide">
          <div className="news_slider">
            <div className="news_slider-text">
              &quot; Chuyển đổi số - Digital Transformation in HCMUTE &quot;
            </div>
          </div>
          <div className="Activity_content">
            <div className="row">
              <div className="col l-9 m-8">
                <div className="Activity_content-title">
                  {Activity.nameActivity}
                </div>
                <div className="Activity_content-date">
                  {new Date(Activity.startDate).toLocaleDateString()} đến{' '}
                  {new Date(Activity.endDate).toLocaleDateString()}
                </div>
                <div className="Activity_content_container">
                  {Activity.description}
                </div>
                <div className="activity_image_space">
                  <img
                    className="activity_image_show"
                    src={Activity.thumbnail}
                  />
                </div>
                <div className="Activity_content_container">
                  - <b>Địa điểm:</b> {Activity.place}
                </div>
                <div className="Activity_content_container">
                  - <b>Quyền lợi:</b> {Activity.benefit}
                </div>
                <div className="Activity_content_container">
                  - <b>Bài viết Fanpage:</b> {Activity.social}
                </div>
                <div className="Activity_button_space">
                  {roleUser === 'user:unionBase' || roleUser === 'admin' ? (
                    <div
                      className="activity-btn-register"
                      onClick={() => {
                        toast.error(
                          'Chức năng này không thuộc phân quyền của User'
                        )
                      }}
                    >
                      Đăng ký
                    </div>
                  ) : (
                    <>
                      {isRegister ? (
                        <div
                          className="activityitem_btn-register"
                          style={{
                            backgroundColor: '#bcbec2',
                            hover: 'enabled',
                          }}
                        >
                          Đã đăng ký
                        </div>
                      ) : (
                        <div
                          className="activityitem_btn-register"
                          onClick={() => {
                            setIdActivityRegister(Activity?._id)
                            setOpenModalConfirm(true)
                          }}
                        >
                          Đăng ký
                        </div>
                      )}
                    </>
                  )}
                </div>
                <div className="Activity_author">{Activity.userCreate}</div>
              </div>
              <div className="col l-3 m-4 c-0">
                <HotNewsItem />
              </div>
            </div>
          </div>
        </div>
        <ModalConfirm
          onOpen={openModalConfirm}
          onClose={() => setOpenModalConfirm(false)}
          title="ĐĂNG KÝ"
          content="Bạn có muốn đăng ký chương trình này ?"
          onOk={() => onHandleRegisterActivity(idActivityRegister)}
        />
      </div>
    )
  }
  return null
}

export default ActivityDetail
