import Icon from '@material-ui/icons/Apps'
import CancelIcon from '@mui/icons-material/Cancel'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import EditIcon from '@mui/icons-material/Edit'
import HourglassBottomIcon from '@mui/icons-material/HourglassBottom'
import QrCodeIcon from '@mui/icons-material/QrCode'
import FileDownloadIcon from '@mui/icons-material/FileDownload'
import { Box, IconButton } from '@mui/material'
import Modal from '@mui/material/Modal'
import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import QRCode from 'react-qr-code'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { fetchActivityRequestForUserBase } from '../../../store/reducers/ActivityReducer'
import NotFound from '../NotFound/NotFound'
import ExportExcelFile from '../ExportExcelFile/ExportExcelFile'
import './ManageRequestActivities.css'

const customStyles = {
  headRow: {
    style: {
      border: 'none',
    },
  },
  headCells: {
    style: {
      color: '#202124',
      fontSize: '16px',
      fontWeight: 500,
    },
  },
  rows: {
    highlightOnHoverStyle: {
      backgroundColor: 'rgba(21, 151, 229, 0.2)',
      borderBottomColor: '#FFFFFF',
      borderRadius: '25px',
      outline: '1px solid #FFFFFF',
    },
    style: {
      fontSize: '14px',
    },
  },
  pagination: {
    style: {
      border: 'none',
      backgroundColor: 'transparent',
    },
  },
}

function ManageRequestActivities() {
  const [open, setOpen] = useState(false)

  const [id, setId] = useState('')
  const data = useSelector((state) => state.activities.ActivitiesRequestBase)
  const dispatch = useDispatch()
  const [pending, setPending] = useState(true)
  const [openExportExcel, setOpenExportExcel] = useState(false)
  const [typeDataExport, setTypeDataExport] = useState('')
  const [slug, setSlug] = useState('')
  const [nameActivity, setNameActivity] = useState('')
  const history = useHistory()

  const columnsGetAll = [
    {
      cell: () => <Icon style={{ fill: 'rgba(21, 151, 229, 0.5)' }} />,
      width: '50px', // custom width for icon button
      style: {
        borderBottom: '1px solid #FFFFFF',
        marginBottom: '-1px',
      },
    },
    {
      name: 'Tên chương trình',
      selector: (row) => row.nameActivity,
      sortable: true,
      grow: 2,
      style: {
        color: '#202124',
        fontSize: '14px',
        fontWeight: 500,
      },
    },
    {
      name: 'Ngày tạo',
      selector: (row) => new Date(row.createdDate).toLocaleDateString(),
      sortable: true,
      center: true,
    },
    {
      name: 'Thể loại',
      selector: (row) => row.tag,
      sortable: true,
      center: true,
    },
    {
      name: 'Trạng thái',
      selector: (row) =>
        row.status ? (
          <CheckCircleIcon color="primary" />
        ) : (
          <HourglassBottomIcon color="warning" />
        ),
      sortable: true,
      center: true,
    },
    {
      name: 'QR Code',
      selector: (row) => (
        <>
          {row.status ? (
            <IconButton
              aria-label="edit"
              color="primary"
              onClick={() => {
                setId(row._id)
                setOpen(true)
              }}
            >
              <QrCodeIcon />
            </IconButton>
          ) : (
            ''
          )}
        </>
      ),

      sortable: true,
      center: true,
    },
    {
      name: 'Danh sách Đăng ký',
      selector: (row) => (
        <>
          {row?.status === true ? (
            <IconButton
              aria-label="edit"
              color="primary"
              onClick={() => {
                setSlug(row.slug)
                setTypeDataExport('register')
                setOpenExportExcel(true)
                setNameActivity(row?.nameActivity)
              }}
            >
              <FileDownloadIcon />
            </IconButton>
          ) : (
            '____'
          )}
        </>
      ),
      grow: 2,
      sortable: true,
      center: true,
    },
    {
      name: 'Danh sách Tham gia',
      selector: (row) => (
        <>
          {row?.status === true ? (
            <IconButton
              aria-label="edit"
              color="primary"
              onClick={() => {
                setSlug(row.slug)
                setTypeDataExport('attendance')
                setOpenExportExcel(true)
                setNameActivity(row?.nameActivity)
              }}
            >
              <FileDownloadIcon />
            </IconButton>
          ) : (
            '____'
          )}
        </>
      ),
      grow: 2,
      sortable: true,
      center: true,
    },
    {
      cell: (row) => (
        <>
          {row.status === false ? (
            <IconButton
              aria-label="edit"
              color="primary"
              href={`/activity/editActivity/${row.slug}`}
            >
              <EditIcon />
            </IconButton>
          ) : (
            '____'
          )}
        </>
      ),
      sortable: true,
      center: true,
    },
  ]
  const onRowClicked = (row) => {
    history.push(`/activity/${row.slug}`)
  }

  const handleClose = () => {
    setOpen(false)
  }
  const handleCloseExportExcel = () => {
    setOpenExportExcel(false)
  }

  useEffect(() => {
    dispatch(fetchActivityRequestForUserBase())
    const timeout = setTimeout(() => {
      setPending(false)
    }, 2000)
    return () => clearTimeout(timeout)
  }, [])

  if (
    localStorage.getItem('authLogin') &&
    JSON.parse(localStorage.getItem('authLogin')).user.role === 'user:unionBase'
  )
    return (
      <div className="request_news">
        <div className="grid wide">
          <div className="request_news_title">
            Danh sách chương trình của bạn
          </div>
          <DataTable
            columns={columnsGetAll}
            data={data}
            pagination
            customStyles={customStyles}
            highlightOnHover
            pointerOnHover
            onRowClicked={onRowClicked}
            progressPending={pending}
          />
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box className="modal_edit_qr">
              <IconButton
                className="modal_container_base_close_btn"
                onClick={handleClose}
              >
                <CancelIcon fontSize="medium" />
              </IconButton>
              <p className="aln-title">QR code hoạt động</p>
              <div className="aln-qr-container">
                <QRCode value={id} />
              </div>
            </Box>
          </Modal>
          <ExportExcelFile
            typeData={typeDataExport}
            isOpen={openExportExcel}
            handelClose={handleCloseExportExcel}
            slug={slug}
            nameActivity={nameActivity}
          />
        </div>
      </div>
    )
  return <NotFound />
}

export default ManageRequestActivities
