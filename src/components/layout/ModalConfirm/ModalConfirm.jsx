import React from 'react'
import Modal from '@mui/material/Modal'
import Box from '@mui/material/Box'
import './ModalConfirm.css'
import IconButton from '@mui/material/IconButton'
import CancelIcon from '@mui/icons-material/Cancel'
import { Button } from '@mui/material'

// eslint-disable-next-line react/prop-types
function ModalConfirm({ onOpen, onClose, title, content, onOk }) {

  return (
    <Modal
      open={onOpen}
      onClose={onClose}
      BackdropProps={{
        style: {
          backgroundColor: 'rgba(182, 186, 183, 0.4)',
          boxShadow: 'none',
        },
      }}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="modal_confirm">
        <IconButton
          className="modal_container_base_close_btn"
          onClick={onClose}
        >
          <CancelIcon fontSize="medium" />
        </IconButton>
        <div className="modal_confirm_title">{title}</div>
        <div className="modal_confirm_content">{content}</div>
        <div className="modal_confirm_space">
          <Button
            color="primary"
            loadingPosition="start"
            variant="outlined"
            className="modal_confirm_button"
            onClick={onClose}
          >
            Hủy
          </Button>
          <Button
            color="primary"
            loadingPosition="start"
            variant="outlined"
            className="modal_confirm_button"
            onClick={onOk}
          >
            Xác nhận
          </Button>
        </div>
      </Box>
    </Modal>
  )
}
export default ModalConfirm
