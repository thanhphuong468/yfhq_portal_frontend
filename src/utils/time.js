import moment from 'moment'

export const getTimeForWeek = (value) => {
  const startDate = new Date(value[0])
  const endDate = new Date(value[1])
  const days = moment(endDate).diff(moment(startDate), 'days')

  let result = []
  for (let i = 0; i < days; i += 1) {
    const date = new Date(moment(endDate).subtract(i, 'days')).getTime()
    result = [...result, date]
  }
  return result
}

export const ggg = () => {}
