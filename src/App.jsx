import { GlobalLoading } from 'components/layout'
import { ConfirmProvider } from 'material-ui-confirm'
import React from 'react'
import { IntlProvider } from 'react-intl'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ROUTES from 'utils/routes'
import './App.css'
import MessengerCustomerChat from 'react-messenger-customer-chat'
import { LicenseInfo } from '@mui/x-data-grid-pro'
import configureStore from './store'
import history from './helper/history'

LicenseInfo.setLicenseKey(
  'x0jTPl0USVkVZV0SsMjM1kDNyADM5cjM2ETPZJVSQhVRsIDN0YTM6IVREJ1T0b9586ef25c9853decfa7709eee27a1e'
)

const store = configureStore()

function App() {
  return (
    <IntlProvider>
      <ConfirmProvider>
        <Provider store={store}>
          <div className="App">
            <Router history={history}>
              <Switch>
                {ROUTES.map((route) => {
                  return (
                    <Route
                      key={route.path}
                      exact={route.exact}
                      path={route.path}
                      component={route.component}
                    />
                  )
                })}
              </Switch>
            </Router>
          </div>
          <ToastContainer hideProgressBar />
          <GlobalLoading />
        </Provider>
        <MessengerCustomerChat
          pageId="244127838945494"
          appId="340058354881904"
        />
        ,
      </ConfirmProvider>
    </IntlProvider>
  )
}

export default App
