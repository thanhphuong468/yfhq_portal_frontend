import React from 'react'
import { Footer, Header, ManageRequestActivities } from '../components/layout'

function ManageRequestActivitiesPage() {
  return (
    <div>
      <Header />
      <ManageRequestActivities />
      <Footer />
    </div>
  )
}

export default ManageRequestActivitiesPage
