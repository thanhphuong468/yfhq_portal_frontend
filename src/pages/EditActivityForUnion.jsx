import { EditActivityForUnion, Footer, Header } from 'components/layout/'
import React from 'react'

function DetailNewsPage() {
  return (
    <div>
      <Header />
      <EditActivityForUnion />
      <Footer />
    </div>
  )
}

export default DetailNewsPage
