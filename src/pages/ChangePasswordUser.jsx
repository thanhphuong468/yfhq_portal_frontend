import React from 'react'
import { ChangePassword, Footer, Header } from 'components/layout'

const ChangePasswordUser = () => {
  return (
    <div style={{ backgroundColor: 'rgba(0,0,0,0.02)' }}>
      <Header />
      <ChangePassword />
      <Footer />
    </div>
  )
}

export default ChangePasswordUser
