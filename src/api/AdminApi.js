import AxiosService from './AxiosService'

class AdminApi {
  getInfoHome = (data) => {
    const url = 'api/v2/admin/getInfoHome'
    return AxiosService.get(url, { params: { data: data.payload } })
  }
}
const adminApi = new AdminApi()
export default adminApi
